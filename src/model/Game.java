package model;

import javafx.application.Platform;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import model.listeners.GameListener;

/**
 * Class for storing a game's data. Created by Varsi on 2015.04.02..
 */
public class Game implements Serializable {
	private static final long serialVersionUID = 9145802610970644384L;
	private static volatile boolean isCounting = false;
	private GameField field;
	private transient GameListener gameListener;
	private transient Timer timer;
	private int time;
	private int mineCount;
	private int turnedUpFields;
	private boolean enabled;
	private boolean minesPlaced;

	private transient TimerTask incrementTime;

	public Game(GameField field) {
		this.field = field;
		mineCount = field.getMineCount();
		turnedUpFields = 0;
		isCounting = true;
		enabled = true;
		minesPlaced = false;

		startTimer();
	}

	public void startTimer() {
		timer = new Timer();
		incrementTime = new TimerTask() {
			@Override
			public void run() {
				if (isCounting) {
					time++;
					if (gameListener != null) {
						Platform.runLater(() -> gameListener.timeUpdated(time));
					}
				} else {
					timer.cancel();
				}
			}
		};
		isCounting = true;
		timer.scheduleAtFixedRate(incrementTime, 0, 1000);
	}

	public static void stopCounting() {
		isCounting = false;
	}

	public GameField getField() {
		return field;
	}

	public void setField(GameField field) {
		this.field = field;
	}

	public GameListener getGameListener() {
		return gameListener;
	}

	public void setGameListener(GameListener gameListener) {
		this.gameListener = gameListener;
		if (gameListener != null) {
			gameListener.mineCountUpdated(mineCount);
		}
	}

	public void turnUp(Point point) {
		if (!minesPlaced) {
			field.placeMines(point);
			minesPlaced = true;
		}
		if (!enabled)
			return;
		Cell c = field.getCell(point);
		if (c.isMarked())
			return;

		if (c.getCellType() == CellType.Mine) {
			timer.cancel();
			enabled = false;
			if (gameListener != null) {
				turnUpAll(false);
				gameListener.gameLost();
			}
		}
		int turnedUp = field.turnUp(point, false);
		turnedUpFields += turnedUp;

		checkWin();
	}

	private void turnUpAll(boolean isWon) {
		for (int i = 0; i < field.getRowCount(); i++) {
			for (int j = 0; j < field.getColumnCount(); j++) {
				Cell c = field.getCell(i, j);
				if (!c.isTurnedUp()) {
					field.turnUp(new Point(i, j), isWon);
				}
			}
		}
	}

	private void checkWin() {
		if (turnedUpFields + field.getMineCount() == field.getRowCount()
				* field.getColumnCount()) {
			// game won
			timer.cancel();
			enabled = false;
			if (gameListener != null) {
				turnUpAll(true);
				gameListener.gameWon(time);
			}
		}
	}

	public void mark(Point p) {
		if (!enabled || field.getCell(p).isTurnedUp())
			return;

		boolean marked = field.mark(p);
		if (marked) {
			mineCount--;
		} else {
			mineCount++;
		}

		if (gameListener != null) {
			gameListener.mineCountUpdated(mineCount);
		}
	}

	public void finish() {
		timer.cancel();
	}
}
