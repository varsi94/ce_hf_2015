package model;

/**
 * Enum for storing the type of the cell.
 * Created by Varsi on 2015.04.01..
 */
public enum CellType {
    Mine, Safe
}
