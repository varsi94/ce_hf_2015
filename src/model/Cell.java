package model;

import java.io.Serializable;

/**
 * Class for storing the data of a cell
 * Created by Varsi on 2015.04.01..
 */
public class Cell implements Serializable {
	private static final long serialVersionUID = 786825302497985633L;
	private CellType cellType;
    private int mineCount;
    private boolean turnedUp;
    private boolean marked;

    public Cell(CellType cellType, int mineCount) {
        this.cellType = cellType;
        this.mineCount = mineCount;
        turnedUp = false;
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public int getMineCount() {
        return mineCount;
    }

    public void setMineCount(int mineCount) {
        this.mineCount = mineCount;
    }

    public boolean isTurnedUp() {
        return turnedUp;
    }

    public void setTurnedUp(boolean turnedUp) {
        this.turnedUp = turnedUp;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }
}
