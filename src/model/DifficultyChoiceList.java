package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for storing difficulty list.
 * Created by Varsi on 2015.04.02..
 */
public abstract class DifficultyChoiceList {
    public static ObservableList<DifficultyItem> getDifficulties() {
        List<DifficultyItem> list = new ArrayList<>();
        list.add(new DifficultyItem(Difficulty.Easy, "Könnyű"));
        list.add(new DifficultyItem(Difficulty.Medium, "Közepes"));
        list.add(new DifficultyItem(Difficulty.Hard, "Nehéz"));
        return FXCollections.observableList(list);
    }

    public enum Difficulty {
        Easy, Medium, Hard
    }

    public static class DifficultyItem {
        private Difficulty difficulty;
        private String label;

        public DifficultyItem(Difficulty difficulty, String label) {
            this.difficulty = difficulty;
            this.label = label;
        }

        public Difficulty getDifficulty() {
            return difficulty;
        }

        @Override
        public String toString() {
            return label;
        }
    }
}
