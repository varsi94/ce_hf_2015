package model.listeners;

/**
 * Listener interface for callbacks.
 * Created by Varsi on 2015.04.02..
 */
public interface GameListener {
    /**
     * Called when the time is updated, to refresh UI.
     *
     * @param sec the time elapsed
     */
    void timeUpdated(int sec);

    /**
     * Called when we found a mine, and lost the game.
     */
    void gameLost();

    /**
     * Called when we won the game.
     * @param time the full time elapsed
     */
    void gameWon(int time);

    /**
     * Called, when the remaining mine count is updated.
     * @param mineCount current mine count
     */
    void mineCountUpdated(int mineCount);
}
