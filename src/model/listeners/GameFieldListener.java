package model.listeners;

import model.Cell;
import model.Point;

/**
 * Interface for callback methods invoked by GameField class.
 * Created by Varsi on 2015.04.08..
 */
public interface GameFieldListener {
	/**
	 * Called when the field is updated
	 * @param point Field coordinate
	 * @param cell Cell
	 * @param isWon determines whether the game is won
	 */
    void fieldUpdated(Point point, Cell cell, boolean isWon);
}
