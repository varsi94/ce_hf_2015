package model;

import java.io.Serializable;
import java.util.*;

import model.listeners.GameFieldListener;

/**
 * Class for storing the data of a gamefield.
 * Created by Varsi on 2015.04.01..
 */
public class GameField implements Iterable<Point>, Serializable {
	private static final long serialVersionUID = 6382986284293453048L;
	private int rowCount;
    private int columnCount;
    private int mineCount;
    private Map<Point, Cell> field;
    private transient GameFieldListener listener;

    private GameField(int rowCount, int columnCount, int mineCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.mineCount = mineCount;
        field = new HashMap<>();
        initField();
    }

    public static GameField getEasyGameField() {
        return new GameField(9, 9, 10);
    }

    public static GameField getMediumGameField() {
        return new GameField(16, 16, 40);
    }

    public static GameField getHardGameField() {
        return new GameField(16, 30, 99);
    }

    private Point[] getNeighbours(Point p) {
        Point[] points = new Point[]{
                new Point(p.getX() - 1, p.getY() - 1),
                new Point(p.getX(), p.getY() - 1),
                new Point(p.getX() + 1, p.getY() - 1),
                new Point(p.getX() - 1, p.getY()),
                new Point(p.getX() + 1, p.getY()),
                new Point(p.getX() - 1, p.getY() + 1),
                new Point(p.getX(), p.getY() + 1),
                new Point(p.getX() + 1, p.getY() + 1)
        };
        return points;
    }

    private List<Cell> getNeighbourCells(Point p) {
        Point[] points = getNeighbours(p);
        List<Cell> result = new ArrayList<>();

        for (Point currPoint : points) {
            try {
                Cell c = getCell(currPoint);
                if (c.getCellType() == CellType.Safe) {
                    result.add(c);
                }
            } catch (IllegalArgumentException ex) {
                //ignore, it is not a valid cell on the field
            }
        }
        return result;
    }

    private List<Point> getNeighbourPoints(Point p) {
        Point[] points = getNeighbours(p);
        List<Point> result = new ArrayList<>();

        for (Point currPoint : points) {
            try {
                Cell c = getCell(currPoint);
                if (c.getCellType() == CellType.Safe) {
                    result.add(currPoint);
                }
            } catch (IllegalArgumentException ex) {
                //ignore, it is not a valid cell on the field
            }
        }
        return result;
    }

    public void placeMines(Point exclude) {
        Random r = new Random();
        for (int i = 0; i < mineCount; i++) {
            Point p = new Point(r.nextInt(rowCount), r.nextInt(columnCount));
            while (p.equals(exclude) || field.get(p).getCellType() == CellType.Mine) {
                p = new Point(r.nextInt(rowCount), r.nextInt(columnCount));
            }
            field.get(p).setCellType(CellType.Mine);
            List<Cell> cells = getNeighbourCells(p);
            for (Cell cell : cells) {
                cell.setMineCount(cell.getMineCount() + 1);
            }

        }
    }

    private void initField() {
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                field.put(new Point(i, j), new Cell(CellType.Safe, 0));
            }
        }
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public int getMineCount() {
        return mineCount;
    }

    public void setMineCount(int mineCount) {
        this.mineCount = mineCount;
    }

    public Cell getCell(int row, int column) {
        if (row >= 0 && row < rowCount && column >= 0 && column < columnCount) {
            return field.get(new Point(row, column));
        } else {
            throw new IllegalArgumentException("Wrong indexes!");
        }
    }

    public Cell getCell(Point point) {
        return getCell(point.getX(), point.getY());
    }

    @Override
    public Iterator<Point> iterator() {
        return field.keySet().iterator();
    }

    public GameFieldListener getListener() {
        return listener;
    }

    public void setListener(GameFieldListener listener) {
        this.listener = listener;
    }

    int turnUp(Point p, boolean isWon) {
        Cell c = getCell(p);
        if (!c.isTurnedUp()) {
            int res = 1;
            c.setTurnedUp(true);
            if (c.getMineCount() == 0) {
                List<Point> neighbours = getNeighbourPoints(p);
                for (Point point : neighbours) {
                    res += turnUp(point, isWon);
                }
            }
            if (listener != null) {
                listener.fieldUpdated(p, c, isWon);
            }
            return res;
        } else {
            return 0;
        }
    }

    boolean mark(Point p) {
        Cell c = getCell(p);
        c.setMarked(!c.isMarked());
        if (listener != null) {
            listener.fieldUpdated(p, c, false);
        }
        return c.isMarked();
    }
}
