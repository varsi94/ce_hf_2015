package views;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;

public class MessagePopup extends Popup {
	public MessagePopup(String line1, String line2) {
		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		Button closeBtn = new Button("Bezár!");
		closeBtn.setOnAction(event -> {
			hide();
		});
		Label label = new Label(line1);
		label.getStyleClass().add("label");
		label.setWrapText(false);

		Label label2 = new Label(line2);
		label2.getStyleClass().add("label");
		label2.setWrapText(false);
		box.getChildren().addAll(label, label2, closeBtn);
		box.setPrefSize(300, 100);
		box.setId("popup");
		getContent().add(box);
		setAutoHide(true);
	}
}
