package views;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;

public class QuestionPopup extends Popup {
	private QuestionListener listener;
	
	public interface QuestionListener {
		public void yesClicked();
	}

	public QuestionPopup(String text, QuestionListener listener) {
		super();
		init(text);
		this.listener = listener;
	}

	private void init(String text) {
		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		Button yesBtn = new Button("Igen");
		yesBtn.setOnAction(event -> {
			hide();
			if (listener != null) {
				listener.yesClicked();
			}
		});
		
		Button noBtn = new Button("Nem");
		noBtn.setOnAction(event -> {
			hide();
		});
		
		HBox hBox = new HBox(10);
		hBox.setAlignment(Pos.CENTER);
		hBox.getChildren().addAll(yesBtn, noBtn);
		Label label = new Label(text);
		label.getStyleClass().add("label");
		label.setWrapText(false);

		box.getChildren().addAll(label, hBox);
		box.setPrefSize(500, 100);
		box.setId("popup");
		getContent().add(box);
		
		setAutoHide(true);
	}
}
