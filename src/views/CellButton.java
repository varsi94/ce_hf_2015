package views;

import javafx.scene.control.Button;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Paint;
import model.Point;

/**
 * CellButton is a Button on the game field. It stores the point where it is.
 * Created by Varsi on 2015.04.10..
 */
public class CellButton extends Button {
	private Point point;

	public CellButton(Point p) {
		super();
		setBorder(new Border(new BorderStroke(Paint.valueOf("black"),
				BorderStrokeStyle.SOLID, new CornerRadii(10),
				BorderWidths.DEFAULT)));
		setPrefSize(1000, 1000);
		setId("default");
		point = p;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}
}
