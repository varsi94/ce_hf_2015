package views;

import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;
import model.*;
import model.listeners.GameFieldListener;

import java.util.HashMap;
import java.util.Map;

/**
 * JavaFX view for displaying the game field. Created by Varsi on 2015.04.08..
 */
public class GameFieldView extends GridPane implements GameFieldListener {
	private static final String MINE_WIN_ID = "mine_win";
	private static final String DEFAULT_ID = "default";
	private static final String MARKED_ID = "marked";
	private static final String TURNED_UP_ID = "turnedUp";
	private static final String MINE_ID = "mine";

	private Game game;
	private Map<Point, CellButton> field;
	private EventHandler<MouseEvent> buttonEventHandler = event -> {
		Point p = ((CellButton) event.getSource()).getPoint();
		if (event.getButton() == MouseButton.SECONDARY) {
			marked(p);
		} else if (event.getButton() == MouseButton.PRIMARY) {
			fieldClicked(p);
		}
	};

	public GameFieldView() {
		field = new HashMap<>();
		widthProperty().addListener((observable, oldValue, newValue) -> {
			setHeight(getNewHeight(newValue));
		});
		heightProperty().addListener((observable, oldValue, newValue) -> {
			setWidth(getNewWidth(newValue));
		});
	}

	private double getNewHeight(Number newValue) {
		return (double) game.getField().getRowCount()
				/ game.getField().getColumnCount() * (double) newValue;
	}

	private double getNewWidth(Number newValue) {
		return (double) game.getField().getColumnCount()
				/ game.getField().getRowCount() * (double) newValue;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		if (this.game != null) {
			// előző leállítása!
			this.game.finish();
		}
		this.game = game;
		clearField();
		if (game != null) {
			updateLayout();
			game.getField().setListener(this);
		}
	}

	private void clearField() {
		for (Point point : field.keySet()) {
			Button btn = field.get(point);
			btn.setText("");
		}
	}

	private void updateRowConstraints() {
		getRowConstraints().clear();
		for (int i = 0; i < game.getField().getRowCount(); i++) {
			RowConstraints rowConstraint = new RowConstraints();
			rowConstraint.setVgrow(Priority.SOMETIMES);
			getRowConstraints().add(rowConstraint);
		}
	}

	private void updateLayout() {
		field.clear();
		getChildren().clear();
		updateRowConstraints();
		updateColumnConstraints();
		setPrefSize(1000 * game.getField().getColumnCount(), 1000 * game
				.getField().getRowCount());

		for (Point p : game.getField()) {
			CellButton btn = new CellButton(p);
			btn.setOnMouseClicked(buttonEventHandler);
			field.put(p, btn);
			add(btn, p.getY(), p.getX());
			fieldUpdated(p, game.getField().getCell(p), false);
		}
	}

	private void updateColumnConstraints() {
		getColumnConstraints().clear();
		for (int i = 0; i < game.getField().getColumnCount(); i++) {
			ColumnConstraints constraints = new ColumnConstraints();
			constraints.setHgrow(Priority.SOMETIMES);
			getColumnConstraints().add(constraints);
		}
	}

	private void marked(Point p) {
		game.mark(p);
	}

	private void fieldClicked(Point p) {
		game.turnUp(p);
	}

	private RotateTransition getRotator(Button b) {
		RotateTransition rotator = new RotateTransition(Duration.millis(750), b);
		rotator.setAxis(Rotate.Y_AXIS);
		rotator.setFromAngle(0);
		rotator.setToAngle(360);
		rotator.setCycleCount(1);
		rotator.setInterpolator(Interpolator.LINEAR);
		return rotator;
	}

	@Override
	public void fieldUpdated(Point point, Cell cell, boolean isWon) {
		Button b = field.get(point);
		if (cell.isTurnedUp()) {
			getRotator(b).play();
			if (cell.getCellType() == CellType.Mine) {
				if (isWon) {
					b.setId(MINE_WIN_ID);
				} else {
					b.setId(MINE_ID);
				}
			} else {
				if (cell.getMineCount() != 0) {
					b.setText("" + cell.getMineCount());
				}
				b.setId(TURNED_UP_ID);
			}
		} else if (cell.isMarked()) {
			b.setId(MARKED_ID);
		} else if (!cell.isMarked()) {
			b.setId(DEFAULT_ID);
		}
	}
}
