package views;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

public class TimeView extends HBox {
	private List<Label> labels;
	private Label doubleDot;
	private String current;
	
	public TimeView() {
		labels = new ArrayList<Label>();
		for (int i = 0; i < 4; i++) {
			labels.add(new Label());
			getChildren().add(i, labels.get(i));
		}

		doubleDot = new Label(":");
		getChildren().add(2, doubleDot);
	}

	private FadeTransition getFadeInTransition(Node n) {
		FadeTransition transition = new FadeTransition(Duration.millis(300), n);
		transition.setInterpolator(Interpolator.LINEAR);
		transition.setFromValue(0);
		transition.setToValue(1);
		return transition;
	}

	private FadeTransition getFadeOutTransition(Node n) {
		FadeTransition transition = new FadeTransition(Duration.millis(300), n);
		transition.setInterpolator(Interpolator.LINEAR);
		transition.setFromValue(1);
		transition.setToValue(0);
		return transition;
	}

	public void setText(String s) {
		int index = 0;
		ParallelTransition fadeOutParalell = new ParallelTransition();
		ParallelTransition fadeInParalell = new ParallelTransition();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ':')
				continue;
			
			if (current == null || current.charAt(i) != s.charAt(i)) {
				FadeTransition fadeOut = getFadeOutTransition(labels.get(index));
				fadeOutParalell.getChildren().add(fadeOut);
				
				labels.get(index).setText(s.charAt(i) + "");
				FadeTransition fadeIn = getFadeInTransition(labels.get(index));
				fadeInParalell.getChildren().add(fadeIn);
			}
			index++;
		}
		
		fadeOutParalell.play();
		fadeInParalell.play();
		
		current = s;
	}
}
