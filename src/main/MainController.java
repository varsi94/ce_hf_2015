package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import model.DifficultyChoiceList;
import model.Game;
import model.GameField;
import model.listeners.GameListener;
import util.FormatUtility;
import views.GameFieldView;
import views.MessagePopup;
import views.QuestionPopup;
import views.TimeView;

/**
 * Main controller. Created by Varsi on 2015.04.02..
 */
public class MainController implements Initializable, GameListener {
	private static final String FILE = "savegame.dat";
	@FXML
	private TimeView timeLabel;
	@FXML
	private ChoiceBox<DifficultyChoiceList.DifficultyItem> difficultyChoiceBox;
	@FXML
	private GameFieldView gameFieldView;
	@FXML
	private Label mineCountLabel;
	private boolean isPlaying;
	private Game currentGame;

	@Override
	public void timeUpdated(int sec) {
		timeLabel.setText(FormatUtility.formatTime(sec));
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		difficultyChoiceBox.getItems().addAll(
				DifficultyChoiceList.getDifficulties());
		difficultyChoiceBox.setValue(difficultyChoiceBox.getItems().get(0));

		if (!loadGame()) {
			currentGame = new Game(GameField.getEasyGameField());
			currentGame.setGameListener(this);
			gameFieldView.setGame(currentGame);
		}
		isPlaying = true;
		Main.getPrimaryStage().setOnCloseRequest(event -> close());
	}

	private boolean loadGame() {
		File f = new File(FILE);
		if (f.exists()) {
			try (ObjectInputStream ois = new ObjectInputStream(
					new FileInputStream(f))) {
				currentGame = (Game) ois.readObject();
				currentGame.setGameListener(this);
				gameFieldView.setGame(currentGame);
				currentGame.startTimer();
			} catch (Exception e) {
				return false;
			}

			f.delete();
			return true;
		}
		return false;
	}

	private void createNewGame() {
		DifficultyChoiceList.DifficultyItem selected = difficultyChoiceBox
				.getSelectionModel().getSelectedItem();
		DifficultyChoiceList.Difficulty diff = selected.getDifficulty();
		switch (diff) {
		case Easy:
			currentGame = new Game(GameField.getEasyGameField());
			break;
		case Medium:
			currentGame = new Game(GameField.getMediumGameField());
			break;
		case Hard:
			currentGame = new Game(GameField.getHardGameField());
			break;
		}
		currentGame.setGameListener(this);
		gameFieldView.setGame(currentGame);
		isPlaying = true;
	}

	@FXML
	private void startNewGame() {
		if (isPlaying) {
			confirmNewGame();
		} else {
			createNewGame();
		}
	}

	private void confirmNewGame() {
		QuestionPopup popup = new QuestionPopup(
				"Biztos, hogy új játékot szeretnél kezdeni?",
				new QuestionPopup.QuestionListener() {

					@Override
					public void yesClicked() {
						createNewGame();
					}
				});
		popup.show(Main.getPrimaryStage());
	}

	@Override
	public void gameLost() {
		MessagePopup popup = new MessagePopup("Vesztettél! :(", "");
		popup.show(Main.getPrimaryStage());
		isPlaying = false;
	}

	@Override
	public void gameWon(int time) {
		MessagePopup popup = new MessagePopup("Gratulálunk, nyertél!", "Időd: "
				+ FormatUtility.formatTime(time));
		popup.show(Main.getPrimaryStage());
		isPlaying = false;
	}

	@Override
	public void mineCountUpdated(int mineCount) {
		mineCountLabel.setText("Aknák:" + mineCount);
	}

	@FXML
	public void close() {
		if (isPlaying) {
			saveGame();
		}
		Main.getPrimaryStage().close();
	}

	private void saveGame() {
		if (isPlaying) {
			try (ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(new File(FILE)))) {
				oos.writeObject(currentGame);
			} catch (FileNotFoundException e) {
				// ignore, mentünk
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
