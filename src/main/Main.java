package main;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Game;

/**
 * Main class for starting the application. Created by Varsi on 2015.04.02..
 */
public class Main extends Application {
	private static Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		Main.primaryStage = primaryStage;
		Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
		Scene scene = new Scene(root, 800, 600);
		scene.getStylesheets().add(
				getClass().getResource("/style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Aknakereső");
		primaryStage.setFullScreen(true);
		primaryStage.setFullScreenExitHint("");
		primaryStage.maximizedProperty().addListener(
				(observable, oldValue, newValue) -> primaryStage
						.setFullScreen(true));
		primaryStage.show();
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		Game.stopCounting();
	}
}
