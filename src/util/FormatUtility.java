package util;

/**
 * Class for some format methods.
 * Created by Varsi on 2015.04.15..
 */
public abstract class FormatUtility {
    public static String formatTime(int time) {
        int min = time / 60;
        int _sec = time % 60;
        String str = ((min < 10) ? "0" : "") + min + ":" + ((_sec < 10) ? "0" : "") + _sec;

        return str;
    }
}
